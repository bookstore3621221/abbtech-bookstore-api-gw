package org.abbtech.dto;

import lombok.Data;
import lombok.Getter;

@Data
public class UserDTO {

    private Long id;
    private String username;
    private String email;
    private String password;


    //private List<UserRole> userRoles;

}
